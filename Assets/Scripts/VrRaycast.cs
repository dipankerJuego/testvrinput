﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VrRaycast : MonoBehaviour
{
    [SerializeField] float fillAmount;

    [SerializeField] Transform vrCamera;
    [SerializeField] GameObject cursor;
    [SerializeField] Image loadingCursor;

    [SerializeField] Text text;
    [SerializeField] Text buttonSelectText;
    [SerializeField] Button button;

    [SerializeField] LayerMask interactable;
    [SerializeField] LayerMask selectable;

    void Start()
    {
        vrCamera = SDKManager.Instance.cameraHead.transform;
    }

    void FixedUpdate()
    {
        LayerMask layerMask = interactable;
        RaycastHit hit;

        Debug.DrawRay(vrCamera.position, vrCamera.forward * 1000, Color.green);

        if (Physics.Raycast(vrCamera.position, vrCamera.TransformDirection(Vector3.forward), out hit, 1000f, layerMask))
        {
            Debug.Log(hit.collider);
            text.text = "Hit";

            //if (!cursor.activeSelf)
                cursor.SetActive(true);

            layerMask = selectable;
            if (Physics.Raycast(vrCamera.position, vrCamera.TransformDirection(Vector3.forward), out hit, 1000f, layerMask))
            {
                Debug.Log("<color=green> called start coroutin</color>");
                StartCoroutine(ObjectSelector());
                layerMask = interactable;
            }
            else
            {
                OnCursorOut();
            }
        }
        else
        {
            text.text = "";
            //if (cursor.activeSelf)
                cursor.SetActive(false);
        }
    }

    IEnumerator ObjectSelector()
    { 
        while((int)loadingCursor.fillAmount != 1)
        {
            loadingCursor.fillAmount += 0.0002f;
            if ((int)loadingCursor.fillAmount == 1)
            {
                buttonSelectText.text = "Selected";
            }
            yield return new WaitForEndOfFrame();
        }
    }
    void OnCursorOut()
    {
        Debug.Log("<color=blue> called stop coroutin</color>");
        //StopCoroutine("ObjectSelector");
        StopAllCoroutines();
        loadingCursor.fillAmount = 0;
        buttonSelectText.text = "";
    }
}
