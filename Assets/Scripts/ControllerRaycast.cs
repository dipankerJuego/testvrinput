﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerRaycast : MonoBehaviour
{
    [SerializeField] GameObject controller;

    [SerializeField] float fillAmount;

    [SerializeField] Transform vrCamera;
    [SerializeField] GameObject cursor;
    [SerializeField] Image loadingCursor;
    [SerializeField] Text buttonSelectText;

    //[SerializeField] LayerMask customMask;

    [SerializeField] LayerMask interactable;
    [SerializeField] LayerMask selectable;
    [SerializeField] Text text;

    [SerializeField] Color c1 = Color.yellow;
    [SerializeField] Color c2 = Color.red;
    [SerializeField] int lengthOfLineRenderer = 20;

    private bool rayState;

    private void Start()
    {
        rayState = false;

        vrCamera = SDKManager.Instance.cameraHead.transform;

        LineRenderer lineRenderer = controller.AddComponent<LineRenderer>();
        lineRenderer.useWorldSpace = false;
        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        lineRenderer.widthMultiplier = 0.02f;
        lineRenderer.positionCount = lengthOfLineRenderer;

        float alpha = 1.0f;
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(c1, 0.0f), new GradientColorKey(c2, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
        );
        lineRenderer.colorGradient = gradient;
    }

    internal void EnableRay(bool value)
    {
        rayState = value;
    }

    private void Update()
    {
#if UNITY_EDITOR
        if(Input.GetKeyUp(KeyCode.Space))
        {
            if (rayState)
            {
                rayState = false;
             }
            else
                rayState = true;
        }
#endif



        RaycastHit hit;
        LayerMask layerMask = interactable;

        if (Physics.Raycast(vrCamera.position, vrCamera.TransformDirection(Vector3.forward), out hit, 1000f, layerMask))
        {
            Debug.Log(hit.collider);
            text.text = "Hit";

            //if (!cursor.activeSelf)
            cursor.SetActive(true);

            layerMask = selectable;
            if (Physics.Raycast(vrCamera.position, vrCamera.TransformDirection(Vector3.forward), out hit, 1000f, layerMask))
            {
                Debug.Log("<color=green> called start coroutin</color>");
                StartCoroutine(ObjectSelector());
                //layerMask = interactable;
            }
            else
            {
                OnCursorOut();
            }
        }
        else
        {
            text.text = "";
            //if (cursor.activeSelf)
            cursor.SetActive(false);
        }


        if (rayState)
        {
            Debug.DrawRay(controller.transform.position, controller.transform.forward * 1000, Color.red);

            LineRenderer lineRenderer = controller.transform.GetComponent<LineRenderer>();
            layerMask = interactable;
            lineRenderer.enabled = true;
            var t = Time.time;
            for (int i = 0; i < lengthOfLineRenderer; i++)
            {
                lineRenderer.SetPosition(i, Vector3.forward * i * 10);
            }

            if (Physics.Raycast(controller.transform.position, controller.transform.TransformDirection(Vector3.forward), out hit, 1000f, layerMask))
            {
                Debug.Log(hit.collider);
                text.text = "Hit";

                //if (!cursor.activeSelf)
                cursor.SetActive(true);

                layerMask = selectable;
                if (Physics.Raycast(vrCamera.position, vrCamera.TransformDirection(Vector3.forward), out hit, 1000f, layerMask))
                {
                    Debug.Log("<color=green> called start coroutin</color>");
                    StartCoroutine(ObjectSelector());
                    layerMask = interactable;
                }
                else
                {
                    OnCursorOut();
                }
            }
            else
            {
                text.text = "";
                cursor.SetActive(false);
            }
        }
        else
        {
            LineRenderer lineRenderer = controller.transform.GetComponent<LineRenderer>();
            lineRenderer.enabled = false;
        }



    }

    IEnumerator ObjectSelector()
    {
        while ((int)loadingCursor.fillAmount != 1)
        {
            loadingCursor.fillAmount += 0.0002f;
            if ((int)loadingCursor.fillAmount == 1)
            {
                buttonSelectText.text = "Selected";
            }
            yield return new WaitForEndOfFrame();
        }
    }
    void OnCursorOut()
    {
        Debug.Log("<color=blue> called stop coroutin</color>");
        //StopCoroutine("ObjectSelector");
        StopAllCoroutines();
        loadingCursor.fillAmount = 0;
        buttonSelectText.text = "";
    }
}
