﻿
//======================================================================================= 
// AUTHOR     : Ippokratis Bournellis
// CREATE DATE     : 10/10/2018
// PURPOSE     : Tumble, zoom and pan the camera around a target object using mouse or touch
// SPECIAL NOTES:   1.Add this script to the camera
//                  2.Drag the target object in the target slot
//                  3.Fine tune the parameters in the inpector
//
// Ippokratis Bournellis 2018, All rights reserved. Medis Media and Medis Media collaborators
// may use this file for internal communication only, any other use, modification, distribution of this file  
// is prohibited by law.  
//========================================================================================

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

    public class TurntablePro : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("Drag a gameObject here, camera will tumble/pan/zoom around it.")]
        private Transform target=null;

        [SerializeField]
        [Tooltip("Camera height offset, if the target is a character it should be around the height of character chest or eyes.")]
        [Space(10)]
        private float cameraHeight = 1.0f;

        [SerializeField]
        [Tooltip("The starting distance between the camera and the target.")]
        private float distance = 4.0f;

        [SerializeField]
        [Space(10)]
        [Tooltip("How close the camera can get to the target.")]
        private float  minDistance = 3.0f;

        [SerializeField]
        [Tooltip("How far the camera can go from the target.")]
        private float maxDistance = 5.0f;

        [SerializeField]
        [Tooltip("Minimum viewing angle from below.")]
        private float minTiltAngle = -15.0f;

        [SerializeField]
        [Tooltip("Maximum viewing angle from above.")]
        private float maxTiltAngle = 60.0f;

        [SerializeField]
        [Space(10)]
        [RangeAttribute (0.5f, 10.0f)]
        [Tooltip("Inertia damping time, for how long the camera wil keep tumbling after you release the mouse button / finish the swipe.")]
        private float inertiaDamping = 5f;

        [SerializeField]
        [Tooltip("How fast the camera zooms and tumbles.")]
        private float sensitivity = 4f;

        [Space(10)]
        [Tooltip("Set to true if you wish the target to auto rotate around itself when the camera does not tumble.")]
        public bool autoRotateTarget = false;

        [SerializeField]
        [Tooltip("How fast the target rotates around itself when the camera does not tumble.")]
        private float targetTurnSpeed = 2.25f;

        private float rotationX, rotationY, rotationXDelta, rotationYDelta, rotationXSmooth, 
        rotationYSmooth, cameraX, cameraY, cameraXSmooth, cameraYSmooth, distanceSmooth, dummy,
        inertiaTemp, inertiaFactor;


        private bool isTumbling =false, canRun = false;

        private Transform m_Transform = null;
        private Transform newTarget = null;

        private Camera m_Camera;

        [SerializeField] bool isNewTarget;
        Vector3 velocity;
        float smoothTime = 0.33f;

        private PhysicsRaycaster m_PhysicsRaycaster;

        // public Text sensitivityDisplay;
        // public Slider sensitivityMult;

        void OnEnable()
        {
            // EventManager.onItemCenter += CenterCamera;
            // EventManager.onToggleIsolate += ToggleIsolate; 
        }

        void OnDisable()
        {
            // EventManager.onItemCenter  -= CenterCamera;
            // EventManager.onToggleIsolate -= ToggleIsolate; 
        }

        void ToggleCanRun(bool m_canRun)
        {
            canRun = m_canRun;
        }

         void ToggleIsolate(bool isIsolated)
        {
            if(isIsolated)
            {
                m_Camera.cullingMask = 1 << 9;//Isolate
                m_PhysicsRaycaster.eventMask = 1 << 9;
            }
            else
            {
                m_Camera.cullingMask = 1 << 0;//Default
                m_PhysicsRaycaster.eventMask = 1 << 0;
            }
        }

        void Start () 
        {
            //Cache the Transform
            m_Transform = GetComponent<Transform>();
            newTarget = new GameObject("newTarget").GetComponent<Transform>();
            Vector3 eulerAngles = m_Transform.eulerAngles;
            rotationX = eulerAngles.x;
            rotationY = eulerAngles.y;
            
            //Cache the camera
            m_Camera = GetComponent<Camera>();

            //Safeguards for proper initialization
            if(target==null&&m_Camera!=null)
            {
                Debug.Log("Please go to the camera inspector and drag a target gameObject in the target slot of the Turntable Plus script");
                canRun = false;
            }
            else
            {
                canRun = true;
            }

            if(m_Camera==null)
            {
                Debug.Log("( " + gameObject.name + " ) is not a camera, please remove the Turntable Plus script from ( " + gameObject.name + " ) and add it to a camera.");
            }

            
            dummy = rotationX = rotationY = rotationXDelta = rotationYDelta = rotationXSmooth = rotationYSmooth = cameraY = cameraX = cameraXSmooth = cameraYSmooth = 0;
            
            //Initial values for smooth intro
            inertiaTemp = inertiaDamping;
            inertiaDamping = 8.0f;
            distanceSmooth = maxDistance * 3.0f;
            isNewTarget = false;
            m_PhysicsRaycaster = GetComponent<PhysicsRaycaster>();
            StartCoroutine(Init());
        }

        //Intro zoom in
        IEnumerator Init()
        {
            yield return new WaitForSeconds(1.5f);
            inertiaDamping = inertiaTemp;
        }

        // void CenterCamera(Transform itemTransform)
        // {
        void CenterCamera(Vector3 itemCenter)
        {
        //    newTarget.position = itemCenter;
        //    isNewTarget = true;
        }
        void FixedUpdate () 
        {
            if (canRun) 
            {
                //------------------------------------------------Input------------------------------------------------------
                //Mouse input is used in standalone bur not in mobile for optimised performance
                //Tumble the camera with the mouse
                #if(UNITY_EDITOR||UNITY_STANDALONE)
               // sensitivity = sensitivityMult.value;
                
                //Scroll wheel zoom - camera dolly
                distance -= Input.GetAxis("Mouse ScrollWheel")*sensitivity*0.5f;

                //Left click to tumble
                if(Input.GetMouseButton(0))
                {
                    rotationYDelta = Input.GetAxis("Mouse X")*sensitivity;
                    rotationXDelta = Input.GetAxis("Mouse Y")*sensitivity;
                    rotationX -= rotationXDelta;
                    rotationY += rotationYDelta;
                    isTumbling = true;
                }
                
                //Right or middle click to pan
                if(Input.GetMouseButton(1)||Input.GetMouseButton(2))
                {
                    float speed = Time.deltaTime*sensitivity*Screen.height*distance*0.00018f; 
                    cameraX -= Input.GetAxis("Mouse X")*speed;
                    cameraY -= Input.GetAxis("Mouse Y")*speed; 
                    isTumbling = true;
                }
                #endif

                //Touch input is available in some desktop/laptops so we do not restrict it to mobile
                //Swipe to tumble the camera
                if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
                {

                    var touchZero = Input.touches[0];
                    float speed = touchZero.deltaTime*sensitivity;
                    rotationYDelta = touchZero.deltaPosition.x*speed;
                    rotationXDelta = touchZero.deltaPosition.y*speed;
                    rotationX -= rotationXDelta;
                    rotationY += rotationYDelta;
                    isTumbling = true;
                }

                //Pinch to zoom  - camera dolly
                if (Input.touchCount == 2)
                {

                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);
                    Vector3 touch0Normalized = touchZero.deltaPosition.normalized;
                    Vector3 touch1Normalized = touchOne.deltaPosition.normalized;
                    float dotProd = (Vector2.Dot(touch0Normalized, touch1Normalized));

                    //Zoom when the fingers open
                    if(dotProd<-0.1f)
                    {
                        //(previousFrameTouchPosition0-previousFrameTouchPosition1)sqrMagnitude
                        float distanceDelta = ((touchZero.position - touchZero.deltaPosition)-(touchOne.position - touchOne.deltaPosition)).sqrMagnitude
                        //(currentFrameTouchPosition0-currentFrameTouchPosition1)sqrMagnitude
                        - (touchZero.position - touchOne.position).sqrMagnitude;
                
                        
                        distance = Mathf.Clamp(distance + distanceDelta*sensitivity*0.00000025f, minDistance, maxDistance);
                    }
                    
                    //Pan when the fingers move and their distance remains realatively the same
                    if(dotProd>0.1f)
                    {
                        if(touchZero.deltaTime>0)
                        {
                            float speed = (Time.deltaTime/touchZero.deltaTime)*sensitivity*Screen.height*0.00000025f;// * sensitivityMult.value;
                            cameraX -= touchZero.deltaPosition.x*speed;
                            cameraY -= touchZero.deltaPosition.y*speed;
                        }
                    }
                }

//                sensitivityDisplay.text = sensitivityMult.value.ToString();

                //------------------------------------------Inertia-----------------------------------------------------
                inertiaFactor = inertiaDamping * 0.01f;

                SmoothDampIt(ref rotationX, ref rotationXSmooth);  
                SmoothDampIt(ref rotationY, ref rotationYSmooth);  

                //Camera Tilt
                if (rotationX < -360f)
                    rotationX += 360f;

                if (rotationX> 360f)
                    rotationX -= 360f;
            
                rotationX = Mathf.Clamp(rotationX, minTiltAngle, maxTiltAngle);

                SmoothDampIt(ref distance, ref distanceSmooth);
//Ugly temp Hack to zoom out               
                // if(isNewTarget)
                // {
                //     minDistance = 0.8f;
                // }
                // else
                // {   
                //     minDistance = 0.3f;
                // }
                distance = Mathf.Clamp(distance, minDistance, maxDistance);

                SmoothDampIt(ref cameraX, ref cameraXSmooth);
                SmoothDampIt(ref cameraY, ref cameraYSmooth);
                Vector3 distanceOffset;
                Vector3 position;
                Quaternion rotation = Quaternion.identity;
                //---------------------------------------Apply transform modifications-----------------------------------
                if(isNewTarget)
                {
                    target.position = Vector3.SmoothDamp(target.position, newTarget.position, ref velocity, smoothTime );
                    if((target.position - newTarget.position).sqrMagnitude<0.00005f)
                    {
                        isNewTarget = false;
                    }
                } 

                rotation = Quaternion.Euler(rotationXSmooth, rotationYSmooth, 0);
                distanceOffset = new Vector3(cameraXSmooth, cameraHeight+cameraYSmooth, -distanceSmooth);
                position = rotation * distanceOffset + target.position ;

                m_Transform.rotation = rotation;
                m_Transform.position = position;

                //Zoom for orthographic cameras
                if(m_Camera.orthographic)
                {
                    m_Camera.orthographicSize = distance;
                }
                //-------------------------------------Automatic turning--------------------------------------------------
                if(autoRotateTarget&&!isTumbling)
                {
                    target.RotateAround(target.position, Vector3.up, -targetTurnSpeed*0.1f);
                }

                isTumbling = false;
            }
        }

        void SmoothDampIt(ref float toSmooth, ref float smoothed)
        {
            smoothed = Mathf.SmoothDamp(smoothed, toSmooth, ref dummy, inertiaFactor);
            dummy = 0.01f;
        }
    }
//  Bounds CalculateBounds(GameObject go) {
//      Bounds b = new Bounds(go.transform.position, Vector3.zero);
//      Object[] rList = go.GetComponentsInChildren(typeof(Renderer));
//      foreach (Renderer r in rList) {
//          b.Encapsulate(r.bounds);
//      }
//      return b;
//  }
//  public void FocusCameraOnGameObject(Camera c, GameObject go) {
//      Bounds b = CalculateBounds(go);
//      Vector3 max = b.size;
//      // Get the radius of a sphere circumscribing the bounds
//      float radius = max.magnitude / 2f;
//      // Get the horizontal FOV, since it may be the limiting of the two FOVs to properly encapsulate the objects
//      float horizontalFOV = 2f * Mathf.Atan(Mathf.Tan(c.fieldOfView * Mathf.Deg2Rad / 2f) * c.aspect) * Mathf.Rad2Deg;
//      // Use the smaller FOV as it limits what would get cut off by the frustum        
//      float fov = Mathf.Min(c.fieldOfView, horizontalFOV);
//      float dist = radius /  (Mathf.Sin(fov * Mathf.Deg2Rad / 2f));
//      Debug.Log("Radius = " + radius + " dist = " + dist);
//      c.transform.SetLocalPositionZ(dist);
//      if (c.orthographic)
//          c.orthographicSize = radius;
     
//      // Frame the object hierarchy
//      c.transform.LookAt(b.center);
//  }


// function FrameObject(cam: Camera, target: GameObject) {
//     // D = R / sin( FOV/2 );
//     if(target) {
//         var c: MeshCollider = target.GetComponent("MeshCollider");
//         if(c) {
//             var r: float = (c.bounds.max - c.bounds.center).magnitude;
//             var fov: float = cam.fieldOfView;
//             var d: float = r / Mathf.Sin( Mathf.Deg2Rad * (fov*0.5) );
//             distance = d + cam.nearClipPlane;
// }