﻿//Cap the frames
// Press Esc to exit the program

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    //Cap the frames
    void OnEnable()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 90;
    }

    // Press Esc to exit the program
    void FixedUpdate()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
}
