﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionTracker : CollisionNotifier
{
    /// <summary>
    /// Causes collisions to stop if the <see cref="GameObject"/> on either side of the collision is disabled.
    /// </summary>
    public bool StopCollisionsOnDisable = true;

    protected List<Collider> trackedCollisions = new List<Collider>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public virtual void StopCollision(Collider collider)
    {
        if ((StatesToProcess & CollisionStates.Exit) == 0)
        {
            return;
        }

        //RemoveDisabledObserver(collider);
        OnCollisionStopped(eventData.Set(this, collider.isTrigger, null, collider));
    }

    protected virtual void OnDisable()
    {
        if (!StopCollisionsOnDisable)
        {
            return;
        }

        foreach (Collider collider in trackedCollisions.ToArray())
        {
            StopCollision(collider);
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        //if ((StatesToProcess & CollisionStates.Enter) == 0)
        //{
        //    return;
        //}

        //AddDisabledObserver(collision.collider);
        OnCollisionStarted(eventData.Set(this, false, collision, collision.collider));
    }

    protected virtual void OnCollisionStay(Collision collision)
    {
        //if ((StatesToProcess & CollisionStates.Stay) == 0)
        //{
        //    return;
        //}

        OnCollisionChanged(eventData.Set(this, false, collision, collision.collider));
    }

    protected virtual void OnCollisionExit(Collision collision)
    {
        //if ((StatesToProcess & CollisionStates.Exit) == 0)
        //{
        //    return;
        //}

        //RemoveDisabledObserver(collision.collider);
        OnCollisionStopped(eventData.Set(this, false, collision, collision.collider));
    }

    protected virtual void OnTriggerEnter(Collider collider)
    {
        //if ((StatesToProcess & CollisionStates.Enter) == 0)
        //{
        //    return;
        //}

        //AddDisabledObserver(collider);
        OnCollisionStarted(eventData.Set(this, true, null, collider));
    }

    protected virtual void OnTriggerStay(Collider collider)
    {
        //if ((StatesToProcess & CollisionStates.Stay) == 0)
        //{
        //    return;
        //}

        OnCollisionChanged(eventData.Set(this, true, null, collider));
    }

    protected virtual void OnTriggerExit(Collider collider)
    {
        //if ((StatesToProcess & CollisionStates.Exit) == 0)
        //{
        //    return;
        //}

        //RemoveDisabledObserver(collider);
        OnCollisionStopped(eventData.Set(this, true, null, collider));
    }


}
