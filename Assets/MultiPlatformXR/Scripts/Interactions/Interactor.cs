﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactor : MonoBehaviour
{
    public delegate void OnCollisionOn(GameObject gameObject);
    public static event OnCollisionOn OnCollisionEvent;

    [SerializeField] private GameObject highlightObject;
    [SerializeField] private ControllerRaycast controllerRaycast;

    public CollisionNotifier.CollisionEventData collisionEventData { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void RaycastLIne(bool active)
    {
        if (controllerRaycast != null)
            controllerRaycast.EnableRay(active);
    }

    public void OnCollisionStarted(CollisionNotifier.CollisionEventData data)
    {
        Debug.Log("OnCollisionStarted " + data.ColliderData.gameObject);
        highlightObject.SetActive(true);
        collisionEventData = data;
    }

    public void OnCollisionChanged(CollisionNotifier.CollisionEventData data)
    {
        Debug.Log("OnCollisionChanged " + data.ColliderData.gameObject);
        highlightObject.SetActive(true);
        collisionEventData = data;
    }

    public void OnCollisionStopped(CollisionNotifier.CollisionEventData data)
    {
        Debug.Log("OnCollisionStopped " + data.ColliderData.gameObject);
        highlightObject.SetActive(false);
        collisionEventData = null;
    }

}
