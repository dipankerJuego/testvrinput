﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionNotifier : MonoBehaviour
{
    /// <summary>
    /// Holds data about a <see cref="CollisionTracker"/> event.
    /// </summary>
    [Serializable]
    public class CollisionEventData : IEquatable<CollisionEventData>
    {
        public Component SourceComponent { get; set; }
        public Collision CollisionData { get; set; }
        public Collider ColliderData { get; set; }
        public bool IsTrigger { get; set; }

        public CollisionEventData Set(CollisionEventData source)
        {
            return Set(source.SourceComponent, source.IsTrigger, source.CollisionData, source.ColliderData);
        }

        public CollisionEventData Set(Component source, bool isTrigger, Collision collision, Collider collider)
        {
            SourceComponent = source;
            IsTrigger = isTrigger;
            CollisionData = collision;
            ColliderData = collider;
            return this;
        }

        public void Clear()
        {
            Set(default, default, default, default);
        }

        public bool Equals(CollisionEventData other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;

            return Equals(SourceComponent, other.SourceComponent) && Equals(ColliderData, other.ColliderData);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return obj is CollisionEventData other && Equals(other);
        }

        public override int GetHashCode()
        {
            return ((SourceComponent != null ? SourceComponent.GetHashCode() : 0) * 397) ^ (ColliderData != null ? ColliderData.GetHashCode() : 0);
        }

        public static bool operator ==(CollisionEventData left, CollisionEventData right) => Equals(left, right);
        public static bool operator !=(CollisionEventData left, CollisionEventData right) => !Equals(left, right);
    }

    [Serializable]
    public class UnityEvent : UnityEvent<CollisionEventData> { }

    [Flags]
    public enum CollisionTypes
    {
        /// <summary>
        /// A regular, non-trigger collision.
        /// </summary>
        Collision = 1 << 0,
        /// <summary>
        /// A trigger collision
        /// </summary>
        Trigger = 1 << 1
    }

    [Flags]
    public enum CollisionStates
    {
        /// <summary>
        /// When a new collision occurs.
        /// </summary>
        Enter = 1 << 0,
        /// <summary>
        /// When an existing collision continues to exist.
        /// </summary>
        Stay = 1 << 1,
        /// <summary>
        /// When an existing collision ends.
        /// </summary>
        Exit = 1 << 2
    }

    public CollisionTypes EmittedTypes = (CollisionTypes)(-1);
    public CollisionStates StatesToProcess = (CollisionStates)(-1);

    public UnityEvent CollisionStarted = new UnityEvent();
    public UnityEvent CollisionChanged = new UnityEvent();
    public UnityEvent CollisionStopped = new UnityEvent();

    /// <summary>
    /// A reused instance to use when raising any of the events.
    /// </summary>
    protected readonly CollisionEventData eventData = new CollisionEventData();
    /// <summary>
    /// A reused instance to use when looking up <see cref="CollisionNotifier"/> components.
    /// </summary>
    protected readonly List<CollisionNotifier> collisionNotifiers = new List<CollisionNotifier>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected virtual void OnCollisionStarted(CollisionEventData data)
    {
        //if ((StatesToProcess & CollisionStates.Enter) == 0 || !CanEmit(data))
        //{
        //    return;
        //}

        CollisionStarted?.Invoke(data);

        foreach (CollisionNotifier notifier in GetNotifiers(data))
        {
            notifier.OnCollisionStarted(data);
        }
    }

    protected virtual void OnCollisionChanged(CollisionEventData data)
    {
        //if ((StatesToProcess & CollisionStates.Stay) == 0 || !CanEmit(data))
        //{
        //    return;
        //}

        CollisionChanged?.Invoke(data);

        foreach (CollisionNotifier notifier in GetNotifiers(data))
        {
            notifier.OnCollisionChanged(data);
        }
    }

    protected virtual void OnCollisionStopped(CollisionEventData data)
    {
        //if ((StatesToProcess & CollisionStates.Exit) == 0 || !CanEmit(data))
        //{
        //    return;
        //}

        CollisionStopped?.Invoke(data);

        foreach (CollisionNotifier notifier in GetNotifiers(data))
        {
            notifier.OnCollisionStopped(data);
        }
    }

    protected virtual bool CanEmit(CollisionEventData data)
    {
        return (data.IsTrigger && (EmittedTypes & CollisionTypes.Trigger) != 0
                || !data.IsTrigger && (EmittedTypes & CollisionTypes.Collision) != 0)
            && (data.SourceComponent == null);
    }

    protected virtual List<CollisionNotifier> GetNotifiers(CollisionEventData data)
    {
        Transform reference = data.ColliderData.GetContainingTransform();

        if (transform.IsChildOf(reference))
        {
            collisionNotifiers.Clear();
        }
        else
        {
            reference.GetComponentsInChildren(collisionNotifiers);
        }

        return collisionNotifiers;
    }
}
