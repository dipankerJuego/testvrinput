﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

public class InputManager : MonoBehaviour
{
    public TestInputs prefab;
    public string[] customAxis;
    private string[] axis;
    public string[] devices;
    public Text text;



    // Start is called before the first frame update
    void Start()
    {
        int key = 330;//JoystickButton0
        for (int i = 0; i < 20; i++)
        {
            TestInputs input = Instantiate(prefab, this.transform);
            input.keyCode = (KeyCode)(key + i);
            input.gameObject.SetActive(true);
        }

        key = 350;//Joystick1Button0
        for (int i = 0; i < 20; i++)
        {
            TestInputs input = Instantiate(prefab, this.transform);
            input.keyCode = (KeyCode)(key + i);
            input.gameObject.SetActive(true);
        }

        key = 370;//Joystick2Button0
        for (int i = 0; i < 20; i++)
        {
            TestInputs input = Instantiate(prefab, this.transform);
            input.keyCode = (KeyCode)(key + i);
            input.gameObject.SetActive(true);
        }

        var axisEnum = System.Enum.GetValues(typeof(AxisType));
        axis = new string[axisEnum.Length];
        for (int i = 0; i < axisEnum.Length; i++)
        {
            axis[i] = InputUtils.GetAxisID((AxisType)axisEnum.GetValue(i));
        }

        devices = XRSettings.supportedDevices;

        XRDevice.deviceLoaded += DeviceLoaded;
    }


    // Update is called once per frame
    void Update()
    {
        if (axis != null && axis.Length > 0)
        {
            for (int i = 0; i < axis.Length; i++)
            {
                if (Input.GetAxis(axis[i]) < 0f || Input.GetAxis(axis[i]) > 0f)
                {
                    Debug.Log(axis[i]);
                    text.text = axis[i];
                }
            }
        }

        if (customAxis != null && customAxis.Length > 0)
        {
            for (int i = 0; i < customAxis.Length; i++)
            {
                if (Input.GetAxis(customAxis[i]) < 0f || Input.GetAxis(customAxis[i]) > 0f)
                {
                    Debug.Log(customAxis[i]);
                    text.text = customAxis[i];
                }
            }
        }



        if (Input.GetKeyDown(KeyCode.Space))
        {
            //var leftHandDevices = new List<UnityEngine.XR.InputDevice>();
            //UnityEngine.XR.InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.LeftHand,
            //                                                 leftHandDevices);
            //if (leftHandDevices.Count == 1)
            //{
            //    UnityEngine.XR.InputDevice device = leftHandDevices[0];
            //    Debug.Log(string.Format("Device name '{0}' with role '{1}'",
            //                            device.name, device.role.ToString()));
            //}
            //else if (leftHandDevices.Count > 1)
            //{
            //    Debug.Log("Found more than one left hand!");
            //}

//            InputDevice left = InputDevices.GetDeviceAtXRNode(XRNode.LeftHand);
        }
    }

    void DeviceLoaded(string value)
    {
        Debug.Log("DeviceLoaded " + value);
    }
}
