﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class XRCameraRig : MonoBehaviour
{
    /// <summary>
    /// Represents the type of physical space available for XR.
    /// </summary>
    public TrackingSpaceType trackingSpaceType = TrackingSpaceType.RoomScale;
    /// <summary>
    /// Automatically set the Unity Physics Fixed Timestep value based on the headset render frequency.
    /// </summary>
    public bool lockPhysicsUpdateRateToRenderFrequency = true;

    protected virtual void OnEnable()
    {
        UpdateTrackingSpaceType();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        UpdateFixedDeltaTime();
    }

    /// <summary>
    /// Updates the tracking space type.
    /// </summary>
    protected virtual void UpdateTrackingSpaceType()
    {
        XRDevice.SetTrackingSpaceType(trackingSpaceType);
    }

    /// <summary>
    /// Updates the fixed delta time to the appropriate value.
    /// </summary>
    protected virtual void UpdateFixedDeltaTime()
    {
        if (lockPhysicsUpdateRateToRenderFrequency
            && Time.timeScale > 0.0f
            && !string.IsNullOrEmpty(XRSettings.loadedDeviceName))
        {
            Time.fixedDeltaTime = Time.timeScale / XRDevice.refreshRate;
        }
    }

    //TODO: Called after has been changed = TrackingSpaceTypeChange
    //TODO: Called after has been changed = LockPhysicsUpdateRateToRenderFrequencyChange
}
