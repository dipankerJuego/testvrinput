﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.IO;

public class CustomMenu : Editor
{
    [MenuItem("CustomTools/Bind Input Key")]
    static void BindInputKey()
    {
        EnforceInputManagerBindings();
    }

    private static void EnforceInputManagerBindings()
    {
        try
        {
            for (int i = 0; i < 28; i++)
            {
                string axisName = "Axis" + (i + 1).ToString();
                BindAxis(new Axis() { name = axisName, type = 2, axis = i, });
            }
        }
        catch
        {
            Debug.LogError("Failed to apply Oculus GearVR input manager bindings.");
        }
    }

    private static void BindAxis(Axis axis)
    {
        SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
        SerializedProperty axesProperty = serializedObject.FindProperty("m_Axes");

        SerializedProperty axisIter = axesProperty.Copy();
        axisIter.Next(true);
        axisIter.Next(true);
        while (axisIter.Next(false))
        {
            if (axisIter.FindPropertyRelative("m_Name").stringValue == axis.name)
            {
                // Axis already exists. Don't create binding.
                return;
            }
        }

        axesProperty.arraySize++;
        serializedObject.ApplyModifiedProperties();

        SerializedProperty axisProperty = axesProperty.GetArrayElementAtIndex(axesProperty.arraySize - 1);
        axisProperty.FindPropertyRelative("m_Name").stringValue = axis.name;
        axisProperty.FindPropertyRelative("descriptiveName").stringValue = axis.descriptiveName;
        axisProperty.FindPropertyRelative("descriptiveNegativeName").stringValue = axis.descriptiveNegativeName;
        axisProperty.FindPropertyRelative("negativeButton").stringValue = axis.negativeButton;
        axisProperty.FindPropertyRelative("positiveButton").stringValue = axis.positiveButton;
        axisProperty.FindPropertyRelative("altNegativeButton").stringValue = axis.altNegativeButton;
        axisProperty.FindPropertyRelative("altPositiveButton").stringValue = axis.altPositiveButton;
        axisProperty.FindPropertyRelative("gravity").floatValue = axis.gravity;
        axisProperty.FindPropertyRelative("dead").floatValue = axis.dead;
        axisProperty.FindPropertyRelative("sensitivity").floatValue = axis.sensitivity;
        axisProperty.FindPropertyRelative("snap").boolValue = axis.snap;
        axisProperty.FindPropertyRelative("invert").boolValue = axis.invert;
        axisProperty.FindPropertyRelative("type").intValue = axis.type;
        axisProperty.FindPropertyRelative("axis").intValue = axis.axis;
        axisProperty.FindPropertyRelative("joyNum").intValue = axis.joyNum;
        serializedObject.ApplyModifiedProperties();
    }

    private class Axis
    {
        public string name = String.Empty;
        public string descriptiveName = String.Empty;
        public string descriptiveNegativeName = String.Empty;
        public string negativeButton = String.Empty;
        public string positiveButton = String.Empty;
        public string altNegativeButton = String.Empty;
        public string altPositiveButton = String.Empty;
        public float gravity = 0.0f;
        public float dead = 0.001f;
        public float sensitivity = 1.0f;
        public bool snap = false;
        public bool invert = false;
        public int type = 2;
        public int axis = 0;
        public int joyNum = 0;
    }
}
