﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInputs : MonoBehaviour
{
    // Right Trackpad Touch JoystickButton17
    // Right Trackpad Press JoystickButton9
    // Right Trigger JoystickButton15
    // Right Grip JoystickButton5
    // Right Menu JoystickButton0

    // Left Trackpad Touch JoystickButton16
    // Left Trackpad Press JoystickButton8
    // Left Trigger JoystickButton14
    // Left Grip JoystickButton4
    // Left Menu JoystickButton2

    public KeyCode keyCode;
    public string buttonName;
    public InputManager inputManager;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = keyCode.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(keyCode))
        {
            Debug.Log(keyCode);
            inputManager.text.text = keyCode.ToString();
        }

        if(!string.IsNullOrEmpty(buttonName) && Input.GetButton(buttonName))
        {
            Debug.Log(buttonName);
            inputManager.text.text = buttonName;
        }
    }
}
