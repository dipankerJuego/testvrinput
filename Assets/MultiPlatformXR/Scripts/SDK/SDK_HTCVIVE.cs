﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class SDK_HTCVIVE : SDKBase
{
    private const string controllerName = "OpenVR Controller(Vive Controller MV)";

    [Header("VIVE Controller")]
    [SerializeField] private ControllerBase leftControler;
    [SerializeField] private ControllerBase rightControler;
    [SerializeField] private Interactor intreactor;

    private Interactor leftInteractor;
    private Interactor rightInteractor;
    private Interactor currentInteractor;

    private bool isTriggeredPressed;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        InitializeController();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        //if(isTriggeredPressed)
        //{
        //    if(currentInteractor != null && currentInteractor.collisionEventData != null)
        //    {
        //        Transform objectInHand = currentInteractor.collisionEventData.ColliderData.transform;
        //        Vector3 handPos = currentInteractor.transform.position;
        //        Vector3 objectPos = objectInHand.position;
        //        float magnitude = (handPos - objectPos).magnitude;

        //        objectInHand.position = handPos + (objectPos-handPos);
        //    }
        //}
        
    }

    #region Controller Input

    public void OnMenuPress(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnMenuPress " + value);
    }

    public void OnTriggerPress(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTriggerPress " + value);
        //leftInteractor.RaycastLIne(value);
        if (type == ControllerType.RightController)
            rightInteractor.RaycastLIne(value);
        //isTriggeredPressed = value;
        //if(type == leftControler.controllerType && value)
        //{
        //    if(leftInteractor.collisionEventData != null && 
        //        leftInteractor.collisionEventData.ColliderData.gameObject.name.Contains("box"))
        //    {
        //        currentInteractor = leftInteractor;
        //    }
        //}
        //if (type == rightControler.controllerType && value)
        //{
        //    Debug.Log(type.ToString() + " OnTriggerPress ****** " + value);
        //    if (rightInteractor.collisionEventData != null && 
        //        rightInteractor.collisionEventData.ColliderData.name.Contains("box"))
        //    {
        //        currentInteractor = rightInteractor;
        //    }
        //}

        //if (value == false)
        //    currentInteractor = null;
    }

    public void OnGripPress(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnGripPress " + value);
    }

    public void OnTrackpadPress(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTrackpadPress " + value);
    }

    public void OnTrackpadTouch(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTrackpadTouch " + value);
    }

    public void OnTrackpadHorizontal(float value, ControllerType type)
    {
        if (value != 0)
            Debug.Log(type.ToString() + " OnTrackpadHorizontal " + value);
    }

    public void OnTrackpadVertical(float value, ControllerType type)
    {
        if (value != 0)
            Debug.Log(type.ToString() + " OnTrackpadVertical " + value);
    }

    public void OnTrackpadMovement(Vector2 value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTrackpadMovement " + value);
    }

    public void OnTriggerSqueeze(float value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTriggerSqueeze " + value);
    }

    #endregion

    protected override void InitializeController()
    {
        base.InitializeController();
        string[] joyStickes = Input.GetJoystickNames();
        if (joyStickes != null && joyStickes.Length > 0)
        {
            foreach (var item in joyStickes)
            {
                Debug.Log(item);
                if (item.Contains(controllerName))
                {
                    if (item.Contains("Left"))
                    {
                        leftInteractor = leftControler.InstantiateInteractor(intreactor);
                        leftInteractor.gameObject.SetActive(true);
                    }
                    if (item.Contains("Right"))
                    {
                        rightInteractor = rightControler.InstantiateInteractor(intreactor);
                        rightInteractor.gameObject.SetActive(true);
                    }
                }
            }
        }
    }
}
