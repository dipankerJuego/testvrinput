﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class SDKManager : MonoBehaviour
{
    [Header("Camera Head Pose")]
    public GameObject cameraHead;
    [Header("Hand Pose")]
    public GameObject leftHand;
    public GameObject rightHand;

    public SDKBase[] allSDK;

    private static SDKManager m_instance;
    private SDKBase CurrentSDK;

    public static SDKManager Instance
    {
        get
        {
            if (m_instance == null)
            {
                //TODO: Create Instance from Prefab....
            }
            return m_instance;
        }
    }

    private void CreateInstance()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        CreateInstance();
        if (CurrentSDK == null)
        {
            LoadSDKFromList();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadSDKFromList()
    {
        int index = 0;
        if (XRSettings.enabled)
        {
            Debug.Log(XRSettings.loadedDeviceName);
            index = Array.FindIndex(allSDK, sdk => sdk.deviceType == GetDeviceType());
        }

        index = index == -1 ? 0 : index;
        LoadSDK(index, allSDK);
    }

    void LoadSDK(int index, params SDKBase[] allSDKs)
    {
        if (allSDKs.Length == 0)
        {
            return;
        }

        if (index < 0 || index >= allSDKs.Length)
        {
            Debug.LogError("Index out of range....");
            return;
        }

        CurrentSDK = allSDKs[index];
        CurrentSDK.gameObject.SetActive(true);
    }

    public static DeviceType GetDeviceType()
    {
        string loadedDeviceName = XRSettings.loadedDeviceName;
        switch (loadedDeviceName)
        {
            case "daydream": return DeviceType.Daydream;
            case "Oculus":
                {
                    if (Application.platform == RuntimePlatform.Android)
                        return DeviceType.Oculus_Mobile;
                    else
                        return DeviceType.Oculus_Standalone;

                }
            case "OpenVR":
                {
                    return DeviceType.OpenVR;
                }
            default: return DeviceType.None;
        }
    }
}
