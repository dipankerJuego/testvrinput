﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SDKBase : MonoBehaviour
{
    public DeviceType deviceType;

    // Start is called before the first frame update
    protected virtual void Start()
    {

    }

    // Update is called once per frame
    protected virtual void Update()
    {

    }

    protected virtual void InitializeController()
    {
        
    }
}
