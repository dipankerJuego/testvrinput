﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SDK_OculusStandalone : SDKBase
{
    [Header("Oculus Rift Controller")]
    [SerializeField] private ControllerBase leftControler;
    [SerializeField] private ControllerBase rightControler;
    [SerializeField] private ControllerBase remoteControler;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    #region Controller Input

    public void OnButtonPress(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnMenuPress " + value);
    }

    public void OnThumbStickPress(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTriggerPress " + value);
    }

    public void OnThumbStickTouch(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnGripPress " + value);
    }

    public void OnIndexTriggerTouch(bool value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTrackpadPress " + value);
    }

    public void OnThumbstickHorizontal(float value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTrackpadHorizontal " + value);
    }

    public void OnThumbstickVertical(float value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTrackpadVertical " + value);
    }

    public void OnIndexTriggerSqueeze(float value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTriggerSqueeze " + value);
    }

    public void OnHandTriggerSqueeze(float value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTriggerSqueeze " + value);
    }

    public void OnThumbStickMovement(Vector2 value, ControllerType type)
    {
        Debug.Log(type.ToString() + " OnTrackpadMovement " + value);
    }

    #endregion

}
