﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControllerButton : MonoBehaviour
{
    [Serializable]
    public class OnJoyStick : UnityEvent<JoyStickType> { }
    [Serializable]
    public class OnJoyStickDown : UnityEvent<JoyStickType> { }
    [Serializable]
    public class OnJoyStickUp : UnityEvent<JoyStickType> { }

    [SerializeField] private JoyStickType joyStick;
    public OnJoyStick onJoyStick = new OnJoyStick();
    public OnJoyStickDown onJoyStickDown =  new OnJoyStickDown();
    public OnJoyStickUp onJoyStickUp = new OnJoyStickUp();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(InputUtils.GetKeyCode(joyStick)))
        {
            if (onJoyStick != null) onJoyStick.Invoke(joyStick);
        }

        if (Input.GetKeyDown(InputUtils.GetKeyCode(joyStick)))
        {
            if (onJoyStickDown != null) onJoyStickDown.Invoke(joyStick);
        }

        if (Input.GetKeyUp(InputUtils.GetKeyCode(joyStick)))
        {
            if (onJoyStickUp != null) onJoyStickUp.Invoke(joyStick);
        }
    }
}
