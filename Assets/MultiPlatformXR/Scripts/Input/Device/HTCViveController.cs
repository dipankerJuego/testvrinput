﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HTCViveController : ControllerBase
{
    /// <summary>
    /// HTC VIVE Controller Joystick ID
    /// </summary>
    private const JoyStickType menuButtonLeft       = JoyStickType.JoystickButton2;
    private const JoyStickType trackpadTouchLeft    = JoyStickType.JoystickButton16;
    private const JoyStickType trackpadPressLeft    = JoyStickType.JoystickButton8;
    private const JoyStickType gripLeft             = JoyStickType.JoystickButton4;
    private const JoyStickType triggerLeft          = JoyStickType.JoystickButton14;
    private const AxisType horizontalAxisLeft       = AxisType.Axis1;
    private const AxisType verticalAxisLeft         = AxisType.Axis2;
    private const AxisType triggerSquashLeft        = AxisType.Axis9;

    private const JoyStickType menuButtonRight      = JoyStickType.JoystickButton0;
    private const JoyStickType trackpadTouchRight   = JoyStickType.JoystickButton17;
    private const JoyStickType trackpadPressRight   = JoyStickType.JoystickButton9;
    private const JoyStickType gripRight            = JoyStickType.JoystickButton5;
    private const JoyStickType triggerRight         = JoyStickType.JoystickButton15;
    private const AxisType horizontalAxisRight      = AxisType.Axis4;
    private const AxisType verticalAxisRight        = AxisType.Axis5;
    private const AxisType triggerSquashRight       = AxisType.Axis10;

    public OnButtonPress onMenuPress        = new OnButtonPress();
    public OnButtonPress onTriggerPress     = new OnButtonPress();
    public OnButtonPress onTrackpadPress    = new OnButtonPress();
    public OnButtonPress onTrackpadTouch    = new OnButtonPress();
    public OnButtonPress onGripPress        = new OnButtonPress();

    public OnAxisVector1 onAxisHorizontal   = new OnAxisVector1();
    public OnAxisVector1 onAxisVertical     = new OnAxisVector1();
    public OnAxisVector1 onTriggerAxis      = new OnAxisVector1();
    public OnAxisVector2 onAxisVector2      = new OnAxisVector2();

    private void OnEnable()
    {
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }
       
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void OnJoyStick(JoyStickType type)
    {
        Debug.Log("OnJoyStick " + gameObject.name + " = " + type.ToString());
    }

    public override void OnJoyStickDown(JoyStickType type)
    {
        Debug.Log("OnJoyStickDown " + gameObject.name + " = " + type.ToString());
        if (type == menuButtonLeft || type == menuButtonRight)
            if (onMenuPress != null) onMenuPress.Invoke(true, controllerType);
        if (type == trackpadPressLeft || type == trackpadPressRight)
            if (onTrackpadPress != null) onTrackpadPress.Invoke(true, controllerType);
        if (type == trackpadTouchLeft || type == trackpadTouchRight)
            if (onTrackpadTouch != null) onTrackpadTouch.Invoke(true, controllerType);
        if (type == gripLeft || type == gripRight)
            if (onGripPress != null) onGripPress.Invoke(true, controllerType);
        if (type == triggerLeft || type == triggerRight)
            if (onTriggerPress != null) onTriggerPress.Invoke(true, controllerType);
            
    }

    public override void OnJoyStickUp(JoyStickType type)
    {
        Debug.Log("OnJoyStickUp " + gameObject.name + " = " + type.ToString());
        if (type == menuButtonLeft || type == menuButtonRight)
            if (onMenuPress != null) onMenuPress.Invoke(false, controllerType);
        if (type == trackpadPressLeft || type == trackpadPressRight)
            if (onTrackpadPress != null) onTrackpadPress.Invoke(false, controllerType);
        if (type == trackpadTouchLeft || type == trackpadTouchRight)
            if (onTrackpadTouch != null) onTrackpadTouch.Invoke(false, controllerType);
        if (type == gripLeft || type == gripRight)
            if (onGripPress != null) onGripPress.Invoke(false, controllerType);
        if (type == triggerLeft || type == triggerRight)
            if (onTriggerPress != null) onTriggerPress.Invoke(false, controllerType);
    }

    public override void OnAxisValue(float value, AxisType type)
    {
        //Debug.Log("OnAxisValue " + gameObject.name + " = " + type.ToString());
        if (type == horizontalAxisLeft || type == horizontalAxisRight)
            if (onAxisHorizontal != null) onAxisHorizontal.Invoke(value, controllerType);

        if (type == verticalAxisLeft || type == verticalAxisRight)
            if (onAxisVertical != null) onAxisVertical.Invoke(value, controllerType);

        if(type == triggerSquashLeft || type == triggerSquashRight)
            if (onTriggerAxis != null) onTriggerAxis.Invoke(value, controllerType);
    }

    public override void OnAxis2DValue(Vector2 value, AxisType type1, AxisType type2)
    {
        //Debug.Log("OnAxis2DValue " + gameObject.name + " = " + value.ToString());
        if ((type1 == horizontalAxisLeft && type2 == verticalAxisLeft) ||
            (type1 == horizontalAxisRight && type2 == verticalAxisRight))
            if (onAxisVector2 != null) onAxisVector2.Invoke(value, controllerType);
    }
}
