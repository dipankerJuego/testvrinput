﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusRemote : ControllerBase
{
    /// <summary>
    /// Oculus Remote Controller Joystick ID
    /// </summary>
    private const JoyStickType button_One = JoyStickType.JoystickButton0;
    private const JoyStickType button_Two = JoyStickType.JoystickButton1;
    private const AxisType dPadVertical = AxisType.Axis6;
    private const AxisType dPadHorizontal = AxisType.Axis5;


    public OnButtonPress onButtonOnePress = new OnButtonPress();
    public OnButtonPress onButtonTwoPress = new OnButtonPress();
    public OnAxisVector1 ondPadVertical = new OnAxisVector1();
    public OnAxisVector1 ondPadHorizontal = new OnAxisVector1();
    public OnAxisVector2 ondPadMovement = new OnAxisVector2();


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void OnJoyStick(JoyStickType type)
    {
        Debug.Log("OnJoyStick " + gameObject.name + " = " + type.ToString());
    }

    public override void OnJoyStickDown(JoyStickType type)
    {
        Debug.Log("OnJoyStickDown " + gameObject.name + " = " + type.ToString());
        if (type == button_One)
            if (onButtonOnePress != null) onButtonOnePress.Invoke(true, controllerType);
        if (type == button_Two)
            if (onButtonTwoPress != null) onButtonTwoPress.Invoke(true, controllerType);
    }

    public override void OnJoyStickUp(JoyStickType type)
    {
        Debug.Log("OnJoyStickUp " + gameObject.name + " = " + type.ToString());
        if (type == button_One)
            if (onButtonOnePress != null) onButtonOnePress.Invoke(false, controllerType);
        if (type == button_Two)
            if (onButtonTwoPress != null) onButtonTwoPress.Invoke(false, controllerType);
    }

    public override void OnAxisValue(float value, AxisType type)
    {
        Debug.Log("OnAxisValue " + gameObject.name + " = " + type.ToString());
        if (type == dPadVertical)
            if (ondPadVertical != null) ondPadVertical.Invoke(value, controllerType);

        if (type == dPadHorizontal)
            if (ondPadHorizontal != null) ondPadHorizontal.Invoke(value, controllerType);
    }

    public override void OnAxis2DValue(Vector2 value, AxisType type1, AxisType type2)
    {
        Debug.Log("OnAxis2DValue " + gameObject.name + " = " + value.ToString());
        if (type1 == dPadHorizontal && type2 == dPadVertical)
            if (ondPadMovement != null) ondPadMovement.Invoke(value, controllerType);
    }
}
