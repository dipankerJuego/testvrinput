﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControllerBase : MonoBehaviour
{
    [Serializable] public class OnButtonPress       : UnityEvent<bool, ControllerType> { }
    [Serializable] public class OnAxisVector1       : UnityEvent<float, ControllerType> { }
    [Serializable] public class OnAxisVector2       : UnityEvent<Vector2, ControllerType> { }

    public delegate void JoyStickPressed(JoyStickType buttonId);
    public delegate void JoyStickDown(JoyStickType buttonId);
    public delegate void JoyStickUp(JoyStickType buttonId);
    public delegate void JoyStickAxis1D(float value, AxisType buttonId);
    public delegate void JoyStickAxis2D(float value, AxisType buttonId1, AxisType buttonId2);

    public static event JoyStickPressed GetButton;
    public static event JoyStickDown GetButtonDown;
    public static event JoyStickUp GetButtonUp;
    public static event JoyStickAxis1D GetAxis1D;
    public static event JoyStickAxis1D GetAxis2D;

    public ControllerType controllerType;
    protected Interactor intreactor;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
    }

    internal Interactor InstantiateInteractor(Interactor prefab)
    {
        Interactor interactor = Instantiate(prefab, controllerType == ControllerType.LeftController ? GetLeftHandPoint() : GetRightHandPoint());
        return interactor;
    }

    public virtual Transform GetLeftHandPoint()
    {
        return SDKManager.Instance.leftHand.transform;
    }

    public virtual Transform GetRightHandPoint()
    {
        return SDKManager.Instance.rightHand.transform;
    }

    #region Events

    public virtual void OnJoyStick(JoyStickType type)
    {
        Debug.Log("OnJoyStick " + gameObject.name + " = " + type.ToString());
    }

    public virtual void OnJoyStickDown(JoyStickType type)
    {
        Debug.Log("OnJoyStickDown " + gameObject.name + " = " + type.ToString());
    }

    public virtual void OnJoyStickUp(JoyStickType type)
    {
        Debug.Log("OnJoyStickUp " + gameObject.name + " = " + type.ToString());
    }

    public virtual void OnAxisValue(float value, AxisType type)
    {
        Debug.Log("OnAxisValue " + gameObject.name + " = " + type.ToString());
    }

    public virtual void OnAxis2DValue(Vector2 value, AxisType type1, AxisType type2)
    {
        Debug.Log("OnAxis2DValue " + gameObject.name + " = " + value.ToString());
    }

    #endregion

    //public static bool
}
