﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusTouchController : ControllerBase
{
    /// <summary>
    /// Oculus Rift Controller Joystick ID
    /// </summary>
    //Left
    private const JoyStickType button_Three                = JoyStickType.JoystickButton1;
    private const JoyStickType primaryThumbstick_Press = JoyStickType.JoystickButton8;
    private const JoyStickType primaryThumbstick_Touch = JoyStickType.JoystickButton16;
    private const JoyStickType primaryIndexTrigger_Touch = JoyStickType.JoystickButton14;
    private const AxisType primaryThumbstick_Horizontal = AxisType.Axis1;
    private const AxisType primaryThumbstick_Vertical = AxisType.Axis2;
    private const AxisType primaryIndexTrigger_Squeeze = AxisType.Axis9;
    private const AxisType primaryHandTrigger_Squeeze = AxisType.Axis11;
    //Right
    private const JoyStickType button_One                  = JoyStickType.JoystickButton0;
    private const JoyStickType secondaryThumbstick_Press = JoyStickType.JoystickButton8;
    private const JoyStickType secondaryThumbstick_Touch  = JoyStickType.JoystickButton17;
    private const JoyStickType secondaryIndexTrigger_Touch = JoyStickType.JoystickButton15;
    private const AxisType secondaryThumbstick_Horizontal = AxisType.Axis4;
    private const AxisType secondaryThumbstick_Vertical = AxisType.Axis5;
    private const AxisType secondaryIndexTrigger_Squeeze = AxisType.Axis10;
    private const AxisType secondaryHandTrigger_Squeeze = AxisType.Axis12;

    public OnButtonPress onButtonPress = new OnButtonPress();
    public OnButtonPress onThumbStickPress = new OnButtonPress();
    public OnButtonPress onThumbStickTouch = new OnButtonPress();
    public OnButtonPress onIndexTriggerTouch = new OnButtonPress();

    public OnAxisVector1 onThumbstickHorizontal = new OnAxisVector1();
    public OnAxisVector1 onThumbstickVertical = new OnAxisVector1();
    public OnAxisVector1 onIndexTriggerSqueeze = new OnAxisVector1();
    public OnAxisVector1 onHandTriggerSqueeze = new OnAxisVector1();
    public OnAxisVector2 onThumbStickMovement = new OnAxisVector2();

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void OnJoyStick(JoyStickType type)
    {
        Debug.Log("OnJoyStick " + gameObject.name + " = " + type.ToString());
    }

    public override void OnJoyStickDown(JoyStickType type)
    {
        Debug.Log("OnJoyStickDown " + gameObject.name + " = " + type.ToString());
        if (type == button_One || type == button_Three)
            if (onButtonPress != null) onButtonPress.Invoke(true, controllerType);
        if (type == primaryIndexTrigger_Touch || type == secondaryIndexTrigger_Touch)
            if (onIndexTriggerTouch != null) onIndexTriggerTouch.Invoke(true, controllerType);
        if (type == primaryThumbstick_Touch || type == secondaryThumbstick_Touch)
            if (onThumbStickTouch != null) onThumbStickTouch.Invoke(true, controllerType);
        if (type == primaryThumbstick_Press || type == secondaryThumbstick_Press)
            if (onThumbStickPress != null) onThumbStickPress.Invoke(true, controllerType);

    }

    public override void OnJoyStickUp(JoyStickType type)
    {
        Debug.Log("OnJoyStickUp " + gameObject.name + " = " + type.ToString());
        if (type == button_One || type == button_Three)
            if (onButtonPress != null) onButtonPress.Invoke(false, controllerType);
        if (type == primaryIndexTrigger_Touch || type == secondaryIndexTrigger_Touch)
            if (onIndexTriggerTouch != null) onIndexTriggerTouch.Invoke(false, controllerType);
        if (type == primaryThumbstick_Touch || type == secondaryThumbstick_Touch)
            if (onThumbStickTouch != null) onThumbStickTouch.Invoke(false, controllerType);
        if (type == primaryThumbstick_Press || type == secondaryThumbstick_Press)
            if (onThumbStickPress != null) onThumbStickPress.Invoke(false, controllerType);
    }

    public override void OnAxisValue(float value, AxisType type)
    {
        Debug.Log("OnAxisValue " + gameObject.name + " = " + type.ToString());
        if (type == primaryThumbstick_Horizontal || type == secondaryThumbstick_Horizontal)
            if (onThumbstickHorizontal != null) onThumbstickHorizontal.Invoke(value, controllerType);

        if (type == primaryThumbstick_Vertical || type == secondaryThumbstick_Vertical)
            if (onThumbstickVertical != null) onThumbstickVertical.Invoke(value, controllerType);

        if (type == primaryHandTrigger_Squeeze || type == secondaryHandTrigger_Squeeze)
            if (onHandTriggerSqueeze != null) onHandTriggerSqueeze.Invoke(value, controllerType);
    }

    public override void OnAxis2DValue(Vector2 value, AxisType type1, AxisType type2)
    {
        Debug.Log("OnAxis2DValue " + gameObject.name + " = " + value.ToString());
        if ((type1 == primaryThumbstick_Horizontal && type2 == primaryThumbstick_Vertical) ||
            (type1 == secondaryThumbstick_Horizontal && type2 == secondaryThumbstick_Vertical))
            if (onThumbStickMovement != null) onThumbStickMovement.Invoke(value, controllerType);
    }
}
