﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControllerAxis2D : MonoBehaviour
{
    [Serializable] public class OnAxisValue2D : UnityEvent<Vector2, AxisType, AxisType> { }

    public AxisType axisType_Horizontal;
    public AxisType axisType_Vertical;
    public OnAxisValue2D axisValue = new OnAxisValue2D();

    private string XAxisID, YAxisID;

    // Start is called before the first frame update
    void Start()
    {
        XAxisID = InputUtils.GetAxisID(axisType_Horizontal);
        YAxisID = InputUtils.GetAxisID(axisType_Vertical);
    }

    // Update is called once per frame
    void Update()
    {
        if (axisValue != null)
        {
            axisValue.Invoke(new Vector2(Input.GetAxis(XAxisID), Input.GetAxis(YAxisID)), axisType_Horizontal, axisType_Vertical);
        }
    }
}
