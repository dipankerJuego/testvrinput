﻿using UnityEngine;

public static class InputUtils
{
    public static KeyCode GetKeyCode(JoyStickType joyStickType)
    {
        switch (joyStickType)
        {
            case JoyStickType.JoystickButton0: return KeyCode.JoystickButton0;
            case JoyStickType.JoystickButton1: return KeyCode.JoystickButton1;
            case JoyStickType.JoystickButton2: return KeyCode.JoystickButton2;
            case JoyStickType.JoystickButton3: return KeyCode.JoystickButton3;
            case JoyStickType.JoystickButton4: return KeyCode.JoystickButton4;
            case JoyStickType.JoystickButton5: return KeyCode.JoystickButton5;
            case JoyStickType.JoystickButton6: return KeyCode.JoystickButton6;
            case JoyStickType.JoystickButton7: return KeyCode.JoystickButton7;
            case JoyStickType.JoystickButton8: return KeyCode.JoystickButton8;
            case JoyStickType.JoystickButton9: return KeyCode.JoystickButton9;
            case JoyStickType.JoystickButton10: return KeyCode.JoystickButton10;
            case JoyStickType.JoystickButton11: return KeyCode.JoystickButton11;
            case JoyStickType.JoystickButton12: return KeyCode.JoystickButton12;
            case JoyStickType.JoystickButton13: return KeyCode.JoystickButton13;
            case JoyStickType.JoystickButton14: return KeyCode.JoystickButton14;
            case JoyStickType.JoystickButton15: return KeyCode.JoystickButton15;
            case JoyStickType.JoystickButton16: return KeyCode.JoystickButton16;
            case JoyStickType.JoystickButton17: return KeyCode.JoystickButton17;
            case JoyStickType.JoystickButton18: return KeyCode.JoystickButton18;
            case JoyStickType.JoystickButton19: return KeyCode.JoystickButton19;
            default: return KeyCode.None;
        }
    }

    public static string GetAxisID (AxisType axisType)
    {
        return axisType.ToString();
        //switch (axisType)
        //{
        //    case AxisType.TrackPad_Horizontal_LeftHand: return InputAxisName.TrackPad_Horizontal_LeftHand;
        //    case AxisType.TrackPad_Horizontal_RightHand: return InputAxisName.TrackPad_Horizontal_RightHand;
        //    case AxisType.TrackPad_Vertical_LeftHand: return InputAxisName.TrackPad_Vertical_LeftHand;
        //    case AxisType.TrackPad_Vertical_RightHand: return InputAxisName.TrackPad_Vertical_RightHand;
        //    case AxisType.Dpad_Horizontal: return InputAxisName.Dpad_Horizontal;
        //    case AxisType.Dpad_Vertical: return InputAxisName.Dpad_Vertical;
        //    case AxisType.IndexTrigger_Left: return InputAxisName.IndexTrigger_Left;
        //    case AxisType.IndexTrigger_Right: return InputAxisName.IndexTrigger_Right;
        //    case AxisType.HandTrigger_Left: return InputAxisName.HandTrigger_Left;
        //    case AxisType.HandTrigger_Right: return InputAxisName.HandTrigger_Right;

        //    default:return "";
        //}
    }
}
