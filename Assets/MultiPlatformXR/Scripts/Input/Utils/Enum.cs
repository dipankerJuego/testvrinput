﻿using UnityEngine;

public enum DeviceType
{
    None,
    OpenVR,
    Oculus_Standalone,
    Oculus_Mobile,
    Daydream,
    MixedReality_Window
}

public enum ControllerType
{
    HeadGear,
    LeftController,
    RightController,
    Remote
}

public enum JoyStickType
{
    None = -1,
    JoystickButton0 = 0,
    JoystickButton1 = 1,
    JoystickButton2,
    JoystickButton3,
    JoystickButton4,
    JoystickButton5,
    JoystickButton6,
    JoystickButton7,
    JoystickButton8,
    JoystickButton9,
    JoystickButton10,
    JoystickButton11,
    JoystickButton12,
    JoystickButton13,
    JoystickButton14,
    JoystickButton15,
    JoystickButton16,
    JoystickButton17,
    JoystickButton18,
    JoystickButton19
}

public enum AxisType
{
    Axis1 = 0,//Axis2D.PrimaryThumbstick // Left Controller Trackpad (2)
    Axis2 = 1,
    Axis3 = 2,//Joystick and Scroll Wheel
    Axis4 = 3,
    Axis5 = 4,
    Axis6,
    Axis7,
    Axis8,
    Axis9,//Axis1D.PrimaryIndexTrigger
    Axis10,//Axis1D.SecondaryIndexTrigger
    Axis11,//Axis1D.PrimaryHandTrigger
    Axis12,//Axis1D.SecondaryHandTrigger
    Axis13,//Axis1D.PrimaryIndexTrigger
    Axis14,//Axis1D.SecondaryIndexTrigger
    Axis15,//Touch.PrimaryThumbRest(0/1)
    Axis16,//Touch.SecondaryThumbRest(0/1)
    Axis17,//
    Axis18,
    Axis19,
    Axis20
}

public static class InputAxisName
{
    public const string TrackPad_Horizontal_LeftHand = "Horizontal_Left";
    public const string TrackPad_Horizontal_RightHand = "Horizontal_Right";
    public const string TrackPad_Vertical_LeftHand = "Vertical_Left";
    public const string TrackPad_Vertical_RightHand = "Vertical_Right";
    public const string Dpad_Horizontal = "Horizontal_DPad";
    public const string Dpad_Vertical = "Vertical_DPad";

    public const string IndexTrigger_Left = "IndexTrigger_Left";
    public const string IndexTrigger_Right = "IndexTrigger_Right";
    public const string HandTrigger_Left = "HandTrigger_Left";
    public const string HandTrigger_Right = "HandTrigger_Right";

}


