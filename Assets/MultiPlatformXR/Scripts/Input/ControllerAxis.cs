﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControllerAxis : MonoBehaviour
{
    [Serializable] public class OnAxisValue : UnityEvent<float, AxisType> { }

    public AxisType axisType;
    public OnAxisValue axisValue = new OnAxisValue();

    private string axisID;

    // Start is called before the first frame update
    void Start()
    {
        axisID = InputUtils.GetAxisID(axisType);
    }

    // Update is called once per frame
    void Update()
    {
        if (axisValue != null) axisValue.Invoke(Input.GetAxis(axisID), axisType);
    }
}
